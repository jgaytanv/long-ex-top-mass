Welcome to the LongExerciseTopMass!

# Facilitators:

1. David Walter. CERN david.walter@cern.ch
2. Beatriz Ribeiro Lopes. CERN beatriz.ribeiro.lopes@cern.ch
3. Markus Seidel. Riga Tech. U. markus.seidel@cern.ch

# Table of contents:

1. [General remarks on the exercise](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-top-mass/-/blob/master/wiki/1.-General-remarks-on-the-exercise.md)
2. [Introduction](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-top-mass/-/blob/master/wiki/2.-Introduction.md)
3. [Towards a top quark mass measurement](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-top-mass/-/blob/master/wiki/3.-Towards-a-top-quark-mass-measurement.md)
4. [Extracting the top quark mass from the b-jet energy spectrum](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-top-mass/-/blob/master/wiki/4.-Extracting-the-top-quark-mass-from-the-b-jet-energy-spectrum.md)
5. [Group presentation](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-top-mass/-/blob/master/wiki/5.-Group-presentation.md)
5. [Additional links](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-top-mass/-/blob/master/wiki/6.-Additional-links.md)